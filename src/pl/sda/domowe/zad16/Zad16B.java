package pl.sda.domowe.zad16;

import java.util.Scanner;

public class Zad16B {
    public static void main(String[] args) {
//        Napisać program w którym użytkownik podaje na początku dwie liczby: początekZakresu oraz koniecZakresu.
//        Upewnij się że początekZakresu < koniecZakresu a następnie pobierz od użytkownika kolejną liczbę - dzielnik.
//        A:Po pobraniu dzielnika wypisz wszystkie liczby od początekZakresu do koniecZakresu które są podzielne przez dzielnik.
//        B: *pobierz od użytkownika iloscDzielnikow, a następnie wczytaj wszystkie dzielniki.
//          Po wczytaniu wszystkich dzielnikow wypisz wszystkie liczby od poczatekZakresu do koniecZakresu które są podzielne przez wszystkie dzielniki.

        //B

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj początek zakresu w postaci liczby całkowitej");
        int poczatekZakresu = scanner.nextInt();
        System.out.println("Podaj koniec zakresu w postaci liczby całkowietej");
        int koniecZakresu = scanner.nextInt();
        if (koniecZakresu < poczatekZakresu) {
            System.out.println("Podaj liczbę większą od " + poczatekZakresu);
            koniecZakresu = scanner.nextInt();
        }

        System.out.println("Podaj ilość dzielników");
        int iloscDzielnikow = scanner.nextInt();
        int tablicaDzielnikow[] = new int[iloscDzielnikow];

        for (int i = 0; i < iloscDzielnikow; i++) {
            System.out.println("Podaj dzielnik nr " + (i + 1) + " w postaci liczby całkowitej");
            tablicaDzielnikow[i] = scanner.nextInt();
            if (tablicaDzielnikow[i] < poczatekZakresu || tablicaDzielnikow[i] > koniecZakresu) {
                System.out.println("Podaj liczbę całkowitą z zakresu od " + poczatekZakresu + " do " + koniecZakresu);
                tablicaDzielnikow[i] = scanner.nextInt();
            }

        }


        System.out.println("Liczby z zadanego zakresu podzielne przez dzielknik/-i");


        for (int j = poczatekZakresu; j < koniecZakresu + 1; j++) {
            int licznik = 0;
            for (int i = 0; i < iloscDzielnikow; i++) {
                if (j % tablicaDzielnikow[i] == 0) {
                    licznik++;
                }
            }
            if (licznik == iloscDzielnikow) {
                System.out.print(j + "; ");
            }

        }
    }
}