package pl.sda.domowe.zad16;

import java.util.Scanner;

public class Zad16 {
    public static void main(String[] args) {
//        Napisać program w którym użytkownik podaje na początku dwie liczby: początekZakresu oraz koniecZakresu.
//        Upewnij się że początekZakresu < koniecZakresu a następnie pobierz od użytkownika kolejną liczbę - dzielnik.
//        A:Po pobraniu dzielnika wypisz wszystkie liczby od początekZakresu do koniecZakresu które są podzielne przez dzielnik.
//        B: *pobierz od użytkownika iloscDzielnikow, a następnie wczytaj wszystkie dzielniki.
//          Po wczytaniu wszystkich dzielnikow wypisz wszystkie liczby od poczatekZakresu do koniecZakresu które są podzielne przez wszystkie dzielniki.

        //A

        Scanner scanner=new Scanner(System.in);
        System.out.println("Podaj początek zakresu w postaci liczby całkowitej");
        int poczatekZakresu=scanner.nextInt();
        System.out.println("Podaj koniec zakresu w postaci liczby całkowietej");
        int koniecZakresu=scanner.nextInt();
        if(koniecZakresu<poczatekZakresu){
            System.out.println("Podaj liczbę większą od " + poczatekZakresu);
            koniecZakresu=scanner.nextInt();
        }
        System.out.println("Podaj dzielnik w postaci liczby całkowitej");
        int dzielnik = scanner.nextInt();
        if (dzielnik<poczatekZakresu || dzielnik>koniecZakresu){
            System.out.println("Podaj liczbę całkowitą z zakresu od " + poczatekZakresu +" do " +koniecZakresu);
            dzielnik =scanner.nextInt();
        }

        System.out.println("Liczby z zadanego zakresu podzielne przez dzielknik "+dzielnik+" :");
        for (int i = poczatekZakresu; i<koniecZakresu+1 ; i++) {
            if (i%dzielnik==0){
                System.out.println(i + "; ");
            }
        }

    }
}
