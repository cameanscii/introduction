package pl.sda.domowe.zad18;

import java.util.Scanner;

public class PotegiDwojki {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj wartość całkowitą N: ");
        int n = scanner.nextInt();
        if (n <= 0) {
            System.out.println("N musi być liczbą całkowitą dodatnią");
            n = scanner.nextInt();
        }

        for (int i = 0; Math.pow(2, (double) i) < n; i++) {
            System.out.print((int)(Math.pow(2, (double) i))+" ");
        }
    }
}