package pl.sda.domowe.zad17;

import java.util.Scanner;

public class CiagABWhileFor {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wartość całkowitą A jako początek zakresu: ");
        int a = scanner.nextInt();
        System.out.println("Podaj wartość całkowitą B jako koniec zakresu: ");
        int b = scanner.nextInt();
        if (b < a) {
            System.out.println("B musi być większe od A. Podaj jeszcze raz wartość całkowitą B końca zakresu");
            b = scanner.nextInt();

        }

        int sumaCiaguFor = 0;
        for (int i = a; i <= b; i++) {
            sumaCiaguFor += i;
        }
        System.out.print(sumaCiaguFor);

        int sumaCiaguWhile = 0;
        int i = a;
        while (i <= b) {
            sumaCiaguWhile += i;
            i++;
        }
        System.out.println(" "+sumaCiaguWhile);
    }
}
