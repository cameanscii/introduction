package pl.sda.domowe.zad19;

import java.util.Random;
import java.util.Scanner;

public class N_IloscLiczbCalkowitych {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Podaj ilość liczb N: ");
        int n = scanner.nextInt();
        if (n <= 0) {
            System.out.println("N musi być liczbą całkowitą dodatnią");
            n = scanner.nextInt();
        }
        int tablicaMateusza[] = new int[n];
        System.out.println("Liczby całkowite:");
        for (int i = 0; i < n ; i++) {
            int liczbaCalkowita = generator.nextInt();
            System.out.print(liczbaCalkowita + "; ");
            tablicaMateusza[i]=liczbaCalkowita;
        }
        int najmniejszy = tablicaMateusza[0];
        int najwiekszy = tablicaMateusza[0];

        for(int i=1; i< tablicaMateusza.length; i++)
        {
            if(tablicaMateusza[i] > najwiekszy)
            {najwiekszy = tablicaMateusza[i];}
            else if (tablicaMateusza[i] < najmniejszy)
            {najmniejszy = tablicaMateusza[i];}

        }
        System.out.println("\n"+"Najmniejszy element w ciągu: " + najmniejszy);

        System.out.println("Najwiekszy element w ciągu: " + najwiekszy);

        System.out.println("Suma największego i najmniejszego elementu w ciągu: " + (najmniejszy+najwiekszy));
        int sumaElementowCiagu = 0;
        for (int i = 0; i < n; i++) {
            sumaElementowCiagu += tablicaMateusza[i];

        }
        System.out.println("Średnia arytmetyczna: "+(sumaElementowCiagu/n));

    }
}
