package pl.sda.domowe.zad4;

public class Zad4 {
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        int c = 3;

        int tymczasowa = a;
        a = b;
        b = c;
        c = tymczasowa;
        System.out.println("a="+a+" "+"b="+b+" "+"c="+c );
    }
}
