package pl.sda.domowe.zad13;

import java.util.Random;
import java.util.Scanner;

public class Zad13_Random {
    public static void main(String[] args) {

//        Napisz aplikację która pobiera od użytkownika N a nastepnie:
//        losuje N liczb całkowitych (dowolny zakres) i wypisuje je na ekran
//        losuje N liczb zmiennoprzecinkowych i wypisuje je na ekran
//        losuje N razy wartość boolean i wypisuje je na ekran
//        pobiera kolejne dwa parametry poczatekZakresu i koniecZakresu i losuje N liczb całkowitych z tego zakresu, a następnie wypisuje je na ekran
//        pobiera kolejne dwa parametry poczatekZakresu i koniecZakresu i losuje N liczb zmiennoprzecinkowych z tego zakresu, a następnie wypisuje je na ekran
//
//        Rozwiąż to zadanie wykorzystując mechanizm losowania liczb z klasy Random oraz z klasy Math (metoda random()).



        Random generator = new Random();
        Scanner scanner = new Scanner(System.in);


        System.out.print("Podaj wartość N:");
        int n = scanner.nextInt();



        System.out.println("Liczby całkowite:");
        for (int i = 0; i < n ; i++) {
            int liczbaCalkowita = generator.nextInt();
            System.out.print(liczbaCalkowita + "; ");
        }
        System.out.println("\nLiczby zmiennoprzecnkowe:");
        for (int i = 0; i < n ; i++) {
            double liczbaZmiennoprzecinkowa = generator.nextDouble();
            System.out.print(liczbaZmiennoprzecinkowa + "; ");
            }

        System.out.println("\nWartości ligiczne Boolean:");
        for (int i = 0; i < n ; i++) {
            boolean wartoscBool = generator.nextBoolean();
            System.out.print(wartoscBool + "; ");
        }


        System.out.print("\nPodaj początek zakresu (liczba całkowita):");
        int p = scanner.nextInt();
        System.out.print("\nPodaj koniec zakresu (liczba cakowita nie mniejsza niż "+(p+n)+") :" );
        int k = scanner.nextInt();
        System.out.println("Liczby całkowite z podanego zakresu:");
        for (int i = 0; i < n ; i++) {
            int liczbaCalkowita = generator.nextInt((k-p))+p;
            System.out.print(liczbaCalkowita + "; ");
        }

        System.out.print("\nPodaj początek zakresu (liczba zmiennowprzecinkowa):");
        double p2 = scanner.nextDouble();
        System.out.print("\nPodaj koniec zakresu (liczba zmiennowprzecinkowa nie mniejsza niż "+(p2+n)+") :" );
        double k2= scanner.nextDouble();
        System.out.println("Liczby całkowite z podanego zakresu:");
        for (int i = 0; i < n ; i++) {
            double liczbaZmiennoprzecinkowa = generator.nextDouble()*(k2-p2)+p2;
            System.out.print(liczbaZmiennoprzecinkowa + "; ");
        }


    }
}
