package pl.sda.domowe.zad13;

import java.util.Random;
import java.util.Scanner;

public class Zad13_Math {


    public static void main(String[] args) {
        Random generator = new Random();
        Scanner scanner = new Scanner(System.in);


        System.out.print("Podaj wartość N:");
        int n = scanner.nextInt();


        System.out.println("Liczby całkowite:");
        for (int i = 0; i < n; i++) {
            int liczbaCalkowita = (int)Math.random()*1000;
            System.out.print(liczbaCalkowita + "; ");
        }
        System.out.println("\nLiczby zmiennoprzecnkowe:");
        for (int i = 0; i < n; i++) {
            double liczbaZmiennoprzecinkowa = Math.random();
            System.out.print(liczbaZmiennoprzecinkowa + "; ");
        }

        System.out.println("\nWartości ligiczne Boolean:");
        for (int i = 0; i < n; i++) {
            double losowanieBoolean = Math.random();
            if (losowanieBoolean <= 0.5) {
                boolean wartoscBool = true;
                System.out.print(wartoscBool + "; ");
            } else if (losowanieBoolean > 0.5) {
                boolean wartoscBool = false;
                System.out.print(wartoscBool + "; ");
            }
        }


        System.out.print("\nPodaj początek zakresu (liczba całkowita):");
        int p = scanner.nextInt();
        System.out.print("\nPodaj koniec zakresu (liczba cakowita nie mniejsza niż " + (p + n) + ") :");
        int k = scanner.nextInt();
        System.out.println("Liczby całkowite z podanego zakresu:");
        for (int i = 0; i < n; i++) {
            int liczbaCalkowita = (int) Math.random() *(k - p) + p;
            System.out.print(liczbaCalkowita + "; ");
        }

        System.out.print("\nPodaj początek zakresu (liczba zmiennowprzecinkowa):");
        double p2 = scanner.nextDouble();
        System.out.print("\nPodaj koniec zakresu (liczba zmiennowprzecinkowa nie mniejsza niż " + (p2 + n) + ") :");
        double k2 = scanner.nextDouble();
        System.out.println("Liczby całkowite z podanego zakresu:");
        for (int i = 0; i < n; i++) {
            double liczbaZmiennoprzecinkowa = Math.random() * (k2 - p2) + p2;
            System.out.print(liczbaZmiennoprzecinkowa + "; ");
        }


    }
}
