package pl.sda.domowe.zad11;

import java.util.Scanner;

public class Zad11 {
    public static void main(String[] args) {
        double x;

        System.out.println("Podaj dochód");
        Scanner y =new Scanner(System.in);
        x = y.nextDouble();

        double z;
        if(x>0&&x<85528){
            z=(x*18)/100-556.02;
            System.out.println("Kwota podatku wynosi "+z+"PLN");
        }else if(x>85528){
            z = ((x-85528)*32)/100+14839.02;
            System.out.println("Kwota podatku wynosi "+z+"PLN");
        }else{
            System.out.println("Podaj liczbę większą od zera");
        }
    }
}
