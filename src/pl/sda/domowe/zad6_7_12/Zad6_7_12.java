package pl.sda.domowe.zad6_7_12;

import java.util.Scanner;

public class Zad6_7_12 {
    public static void main(String[] args) {
        int waga;
        int wzrost;
        int wiek;

        System.out.println("Podaj swoją wagę");
        Scanner waga2 = new Scanner(System.in);
        waga = waga2.nextInt();
        if (waga <= 0) {
            System.out.println("Podaj liczbę większą od zero");
            Scanner waga3 = new Scanner(System.in);
            waga = waga3.nextInt();
        }


        System.out.println("Podaj swój wzrost");
        Scanner wzrost2 = new Scanner(System.in);
        wzrost = wzrost2.nextInt();
        if (wzrost <= 0) {
            System.out.println("Podaj liczbę większą od zero");
            Scanner wzrost3 = new Scanner(System.in);
            wzrost = wzrost3.nextInt();
        }


        System.out.println("Podaj swój wiek");
        Scanner wiek2 = new Scanner(System.in);
        wiek = wiek2.nextInt();
        if (wiek <= 0) {
            System.out.println("Podaj liczbę większą od zero");
            Scanner wiek3 = new Scanner(System.in);
            wiek = wiek3.nextInt();
        }


        if (waga < 180 && wzrost > 150 && wzrost < 220 && 10 < wiek && wiek < 80) {
            System.out.println("Możesz wejść na kolejkę");
        } else {
            if (waga > 180) {
                System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt ciężki/a");
            }
            if (wzrost < 150 || wzrost > 220) {
                System.out.println("Nie możesz wejść na kolejkę z powodu twojego wzrostu");
            }
            if (wiek < 10 || wiek > 80) {
                System.out.println("Nie możesz wejść na kolejkę z powodu twojego wieku");
            }

        }

    }
}
