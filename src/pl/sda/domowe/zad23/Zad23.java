package pl.sda.domowe.zad23;

import java.util.Scanner;

public class Zad23 {
    public static void main(String[] args) {

        System.out.println("Podaj liczbę:");

        Scanner scanner = new Scanner(System.in);
        int liczba = scanner.nextInt();


        System.out.println("Jej dzielniki to: ");
        for (int i = 1; i < liczba+1; i++) {

            if (liczba%i == 0){
                System.out.print(i + ", ");
            }

        }

    }

}
