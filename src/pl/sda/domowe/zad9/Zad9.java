package pl.sda.domowe.zad9;

public class Zad9 {
    public static void main(String[] args) {
        double stopnieCelsjusza = 22.0;
        double stopnieFahrenheita = 1.8*stopnieCelsjusza+32.0;
        //"%.2f"
        //stopnieFahrenheita = Math.round(stopnieFahrenheita*100)/100.0;

        System.out.format("%.2f \n", stopnieFahrenheita);
    }
}
