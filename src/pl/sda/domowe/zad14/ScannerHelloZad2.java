package pl.sda.domowe.zad14;

import java.util.Scanner;

public class ScannerHelloZad2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj wartość N:");
        int n = scanner.nextInt();
        System.out.println("Petla for");
        for (int i = 0; i <  n; i++) {
            System.out.print(i+1+". Hello World ");
        }

        System.out.println("\nPetla while");
        int i=0;
        while (i<n){
            System.out.print(i+1+". Hello World ");
            i++;
        }

    }
}
