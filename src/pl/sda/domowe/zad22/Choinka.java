package pl.sda.domowe.zad22;

import java.util.Scanner;

public class Choinka {
    public static void main(String[] args) {


        System.out.println("Podaj wysokość N dla choinki (liczba całkowita większa od zera!): ");

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        1
//        3
//        5
//        7
//        9

        int k = n-1 ;

        for (int i = 0; i < n * 2; i++) {

            if (i % 2 == 1) {

                for (int m = 0; m < k; m++) {
                    System.out.print(" ");
                }

                for (int j = 0; j < i; j++) {
                    System.out.print("*");
                }

                System.out.println("");
                k--;
            }


        }


    }
}
