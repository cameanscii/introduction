package pl.sda.domowe.zad10;

public class Zad10 {
    public static void main(String[] args) {
        int a =1;
        int b=2;
        int c=3;
        System.out.println(a+" "+b+" "+ c);

        if(a>=b && a>=c){
            System.out.println("Największa liczba to "+a);
        } else if(b>=a && b>=c){
            System.out.println("Największa liczba to "+b);
        } else {
            System.out.println("Największa liczba to "+c);
        }

        if(a<=b && a<=c){
            System.out.println("Najmniejsza liczba to "+a);
        } else if(b<=a && b<=c){
            System.out.println("Najmniejsza liczba to "+b);
        } else {
            System.out.println("Najmniejsza liczba to "+c);
        }
    }
}
