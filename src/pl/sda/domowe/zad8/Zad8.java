package pl.sda.domowe.zad8;

public class Zad8 {
    public static void main(String[] args) {
        double ocena_matematyka = 5.0;
        double ocena_chemia = 1.0;
        double ocena_j_polski = 2.0;
        double ocena_j_angielski = 4.5;
        double ocena_wos = 4.5;
        double ocena_informatyka = 6.0;
        double srednia_ocen= (ocena_chemia+ocena_informatyka+ocena_j_angielski+ocena_j_polski+ocena_matematyka+ocena_wos)/6;
        double srednia_ocen_scisle=(ocena_chemia+ocena_informatyka+ocena_matematyka)/3;
        double srednia_ocen_humanistyczne=(ocena_j_angielski+ocena_j_polski+ocena_wos)/3;
        System.out.println("Średnia ocen = " + Math.round(srednia_ocen*100)/100.00);
        System.out.println("Średnia ocen z przedmiotów ścisłych= " + Math.round(srednia_ocen_scisle*100)/100.00);
        System.out.println("Średnia ocen z przedmiotów humanistycznych = " + Math.round(srednia_ocen_humanistyczne*100)/100.00);

        if(ocena_matematyka == 1.0){
            System.out.println("Ocena z Matematyki jest niedostateczna.");
        }
        if(ocena_chemia == 1.0){
            System.out.println("Ocena z Chemi jest niedostateczna.");
        }
        if(ocena_j_polski == 1.0){
            System.out.println("Ocena z Języka Polskiego jest niedostateczna.");
        }
        if(ocena_j_angielski == 1.0){
            System.out.println("Ocena z Języka Angielskiego jest niedostateczna.");
        }
        if(ocena_wos == 1.0){
            System.out.println("Ocena z WOS jest niedostateczna.");
        }
        if(ocena_informatyka == 1.0){
            System.out.println("Ocena z Informatyki jest niedostateczna.");
        }
        if(srednia_ocen_humanistyczne == 1.0){
            System.out.println("Średnia ocen z przedmiotów humanistycznych jest niedostateczna.");
        }
        if(srednia_ocen_scisle == 1.0){
            System.out.println("Średnia ocen z przedmiotów ścisłych jest niedostateczna.");
        }

    }
}
