package pl.sda.wprowadzenie.zad6;

public class KonwersjaTypow {
    public static void main(String[] args) {
        //a
        short zmiennaShort = 5;
        int zmiennaInt = zmiennaShort;
        System.out.println(zmiennaInt);
        //b
        long zmiennaLong = zmiennaShort;
        System.out.println(zmiennaLong);

        //c
        float zmiennaFloat = zmiennaInt;
        System.out.println(zmiennaFloat);

        //d
        double zmiennaDouble = zmiennaInt;
        System.out.println(zmiennaDouble);

        //e

        int zmiennaInt2 = (int)zmiennaLong;
        System.out.println(zmiennaInt2);

        //f
        byte zmiennaByte = (byte)zmiennaShort;
        System.out.println(zmiennaByte);

        //g
        char zmiennaChar = 'A';
        int zmiennaInt3 = zmiennaChar;
        System.out.println(zmiennaInt3);




    }
}
