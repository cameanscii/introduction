package pl.sda.wprowadzenie.zad7;

public class WyrazenieIfElse {
    public static void main(String[] args) {
        //a
        if (2 > 3) {
            System.out.println(":) 2>3");
        } else {
            System.out.println(":( 2>3");
        }

        //b
        if (4 < 5) {
            System.out.println(":) 4<5");
        } else {
            System.out.println(":( 4<5");
        }

        //c
        if (2 - 2 == 0) {
            System.out.println(":) 2-2==0");
        } else {
            System.out.println(":( 2-2==0");
        }

        //d
        if (true) {
            System.out.println(":) true");
        } else {
            System.out.println(":( true");
        }

        //e
        if (9 % 2 == 0) {
            System.out.println(":) 9%2==0");
        } else {
            System.out.println(":( 9%2==0");
        }

        //f
        if (9 % 3 == 0) {
            System.out.println(":) 9%3==0");
        } else {
            System.out.println(":( 9%3==0");
        }


    }
}
