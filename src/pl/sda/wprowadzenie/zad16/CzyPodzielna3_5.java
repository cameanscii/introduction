package pl.sda.wprowadzenie.zad16;

public class CzyPodzielna3_5 {
    public static void main(String[] args) {
        System.out.println(metodaPodzielne(11));
    }

    public static boolean metodaPodzielne (int a){
        return (a%3 ==0 || a%5 ==0);
    }
}
