package pl.sda.wprowadzenie.zad14;

import java.util.Random;

public class Tablice_Aplikacja {
    public static void main(String[] args) {
        int tablicaMateusza[] = new int[10];
        Random generator = new Random();
        for (int i = 0; i < tablicaMateusza.length; i++) {
            int wygenerowana = generator.nextInt(20) - 10;
            tablicaMateusza[i] = wygenerowana;
        }

        System.out.println("Zawartosc tablicy ");
        for (int i = 0; i < tablicaMateusza.length; i++) {
            System.out.print(tablicaMateusza[i] + "; ");
        }

        int najmniejszy = tablicaMateusza[0];
        int najwiekszy = tablicaMateusza[0];

        for(int i=1; i< tablicaMateusza.length; i++)
        {
            if(tablicaMateusza[i] > najwiekszy)
            {najwiekszy = tablicaMateusza[i];}
            else if (tablicaMateusza[i] < najmniejszy)
            {najmniejszy = tablicaMateusza[i];}

        }
        System.out.println("\n"+"Najmniejszy element w tablicy: " + najmniejszy);

        System.out.println("Najwiekszy element w tablicy: " + najwiekszy);
    }


}

