package pl.sda.wprowadzenie.zad9;

import java.util.Scanner;

public class Petle_1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        System.out.println("Podaj liczbe");
        int liczba = scanner.nextInt();


        System.out.println("Podaj dzielnik");
        int dzielnik = scanner.nextInt();

        for (int i = 1; i < liczba; i++) {
            if (i % dzielnik == 0) {
                System.out.println("Liczba " + i + " jest podzielna przez " + dzielnik);

            }
        }

    }
}
