package pl.sda.wprowadzenie.zad10;

public class Petle_2 {
    public static void main(String[] args) {
        //a
        System.out.println("Podpunkt A");
        for (int i = 1; i < 101 ; i++) {
            System.out.println(i);
        }
        //b
        System.out.println("Podpunkt B");
        for (int i = 1000; i < 1021 ; i++) {
            System.out.print(i+", ");
        }
        //c
        System.out.println("\n"+"Podpunkt C");
        for (int i = -30; i < 1001; i++)
            if (i % 5 == 0) {
                System.out.println(i);
            }
        //d
        System.out.println("Podpunkt D");
        for (int i = 1; i < 101; i++) {
            if(i%3==0) {
                System.out.println(i);
            }
        }

        //e
        System.out.println("Podpunkt E");
        for (int i = 30; i < 301; i++) {
            if(i%3==0&&i%5==0) {
                System.out.println(i);
            }
        }
        //f
        System.out.println("Podpunkt F");
        for (int i = -300; i < 301 ; i++) {
            if(i%2!=0) {
                System.out.print(i+"; ");
            }
        }

        //g
        System.out.println("\n"+"Podpunkt G");
        for (int i = -100; i < 101; i++) {
            if (i % 2 == 0) {
                System.out.println(i + "; ");
            }
        }

        //h
        System.out.println("\n"+"Podpunkt H");
        for (int i = 97; i < 123; i++) {

            System.out.print((char)i+" ");

        }

        //i
        System.out.println("\n"+"Podpunkt I");
        for (int i = 65; i < 91; i++) {

            System.out.print((char)i+" ");

        }

        //j
        System.out.println("\n"+"Podpunkt J");
        for (int i = 65; i < 91; i = i + 2) {

            System.out.print((char)i+" ");

        }

        //k
        System.out.println("\n"+"Podpunkt K");
        for (int i = 98; i < 123; i = i +2) {
            if(i%5==0){
                System.out.print((char)i+" ");
            }

        }

        //l
        System.out.println("\n"+"Podpunkt L");
        for (int i = 1; i < 101 ; i++) {
            System.out.println(i+". Hello World");

        }




    }
}
