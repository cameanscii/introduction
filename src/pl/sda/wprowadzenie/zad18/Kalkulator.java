package pl.sda.wprowadzenie.zad18;

import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {
    kalkulator();
    }

    public static void kalkulator() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rodzaj działania za pomocą symbolu: * lub / lub + lub - ");
        String slowo = scanner.nextLine();
        char znak = slowo.charAt(0);
        System.out.println("Podaj pierwszą liczbę");
        int y = scanner.nextInt();
        System.out.println("Podaj drugą liczbę");
        int z = scanner.nextInt();
        switch (znak) {
            case '*':
                System.out.println("Wynik mnożenia liczb " + y +" i "+z+" wynosi: " + mnozenie(y, z));
                break;

            case '/':
                System.out.println("Wynik dzielenia liczb " + y +" i "+z+" wynosi: "+dzielenie(y, z));
                break;

            case '+':
                System.out.println("Wynik dodawanie liczb " + y +" i "+z+" wynosi: "+dodawanie(y, z));
                break;
            case '-':
                System.out.println("Wynik odejmowanie liczb " + y +" i "+z+" wynosi: "+odejmowanie(y, z));
                break;


            default:
                System.out.println("nieprzewidziana sytuacja");
        }


    }

    public static int mnozenie(int a, int b) {
        return a*b;

    }

    public static double dzielenie(int a, int b) {
        return (double)a/b;
    }

    public static int dodawanie(int a, int b) {
            return a+b;
    }

    public static int odejmowanie(int a, int b) {
        return a-b;
    }
}

