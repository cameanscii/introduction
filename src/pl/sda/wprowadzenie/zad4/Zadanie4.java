package pl.sda.wprowadzenie.zad4;

public class Zadanie4 {
    public static void main(String[] args) {
        //a
        System.out.println(2+3);
        //b
        System.out.println(2-4);
        //c
        System.out.println(5/2);
        //d
        System.out.println(5.0/2);
        //e
        System.out.println(5/2.0);
        //f
        System.out.println(5.0/2.0);
        //g
        System.out.println(100L-10);
        //h
        System.out.println(2f-3);
        //i
        System.out.println(5f/2);
        //j
        System.out.println(5d/2);
        //k
        System.out.println('A'+2);
        //l
        System.out.println('a'+2);
        //m
        System.out.println("a"+2);
        //n
        System.out.println("a"+"b");
        //o
        System.out.println('a'+'b');
        //p
        System.out.println("a"+'b');
        //q
        System.out.println("a"+'b'+3);
        //r
        System.out.println('b'+3+"a");
        //s
        System.out.println(9%4);
        //modulo rozpisane recznie
        System.out.println(9-(4*(9/4)));

    }

}
