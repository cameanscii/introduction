package pl.sda.wprowadzenie.zad11;

public class Petle_While {
    public static void main(String[] args) {
        //a
        System.out.println("Podpunkt A");
        int i = 1;
        while (i < 101) {
            System.out.println(i);
            i++;
        }
        //b
        System.out.println("Podpunkt B");
        i = 1000;
        while (i < 1021) {
            System.out.print(i + ", ");
            i++;
        }
        //c
        System.out.println("\n" + "Podpunkt C");
        i = -30;
        while (i < 1001) {
            if (i % 5 == 0) {
                System.out.println(i);
            }
            i++;
        }


        //d
        System.out.println("Podpunkt D");
        i = 1;
        while (i < 101) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
            i++;
        }
        //e
        System.out.println("Podpunkt E");
        i = 30;
        while (i < 301) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println(i);
            }
            i++;
        }

        //f
        System.out.println("Podpunkt F");
        i = -300;
        while (i < 301) {
            if (i % 2 != 0) {
                System.out.print(i + "; ");
            }
            i++;
        }


        //g
        System.out.println("\n" + "Podpunkt G");
        i = -100;
        while (i < 101) {
            if (i % 2 == 0) {
                System.out.println(i + "; ");
            }
            i++;
        }

        //h
        System.out.println("\n" + "Podpunkt H");
        i = 97;
        while (i < 123) {
            System.out.print((char) i + " ");
            i++;
        }

        //i
        System.out.println("\n" + "Podpunkt I");
        i = 65;
        while (i < 91) {
            System.out.print((char) i + " ");
            i++;
        }

        //j
        System.out.println("\n" + "Podpunkt J");
        i = 65;
        while (i < 91) {
            System.out.print((char) i + " ");
            i = i + 2;
        }


        //k
        System.out.println("\n" + "Podpunkt K");
        i = 98;
        while (i < 123) {
            System.out.print((char) i + " ");
            i = i + 2;
        }


        //l
        System.out.println("\n" + "Podpunkt L");
        i=1;
        while(i<101){
            System.out.println(i + ". Hello World");
            i++;
        }


    }
}