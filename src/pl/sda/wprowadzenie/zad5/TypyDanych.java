package pl.sda.wprowadzenie.zad5;

public class TypyDanych {
    public static void main(String[] args) {
        //a
        System.out.println(false==false);
        //b
        System.out.println(false != true);
        //c
        System.out.println(!true);
        //d
        System.out.println(2>4);
        //e
        System.out.println(3>5);
        //f
        System.out.println(3==3&&3==4);
        //g
        System.out.println(3!=5||3==5);
        //h
        System.out.println((2+4)>(1+3));
        //i
        System.out.println("cos".equals("cos"));
        //j
        System.out.println("cos"=="cos");
    }
}
