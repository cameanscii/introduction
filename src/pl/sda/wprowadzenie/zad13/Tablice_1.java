package pl.sda.wprowadzenie.zad13;

public class Tablice_1 {
    public static void main(String[] args) {
        int mojaTablica[] = {1, 3, 5, 10};
        System.out.println("Wypisz 2 elementy");
        System.out.println(mojaTablica[0]);
        System.out.println(mojaTablica[1]);
        System.out.println("Wypisz elementy tablicy");
        for (int i = 0; i < mojaTablica.length; i++) {
            System.out.println(mojaTablica[i]);
        }
        System.out.println("Wypisz liczby o parzystym indeksie");
        for (int i = 0; i < mojaTablica.length; i++) {
            if (i % 2 == 0) {
                System.out.println(mojaTablica[i]);
            }
        }
        System.out.println("Wypisz liczby parzyste");
        for (int i = 0; i < mojaTablica.length; i++) {
            if (mojaTablica[i] % 2 == 0) {
                System.out.println(mojaTablica[i]);
            }
        }
        System.out.println("Wypisz elementy tablicy w odwroconej kolejnosci");
        for (int i = (mojaTablica.length-1); i >= 0; i--) {
            System.out.println(mojaTablica[i]);
        }

    }
}
