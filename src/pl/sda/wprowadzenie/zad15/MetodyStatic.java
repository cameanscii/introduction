package pl.sda.wprowadzenie.zad15;

public class MetodyStatic {
    public static void main(String[] args) {
        drukujParzystoscLiczby(5);
        boolean x=czyLiczbaJestParzysta(2);
        System.out.println(x);

    }
    public static void drukujParzystoscLiczby(int a){
            if(a%2==0){
                System.out.println("Liczba jest parzysta");

            }else{
                System.out.println("Liczba nieparzysta");
            }

        }

        public static boolean czyLiczbaJestParzysta(int a){
            if(a%2==0){
                return true;
            }
            //nie trzeba robic elsa bo na pierwszym returnie ucina metode
            return false;
        }

        /* najprostrzsza wersja tej metody:
        public static boolean czyLiczbaJestParzysta(int a){
            return a%2==0;
            }


         */

    }



