package pl.sda.wprowadzenie.zad8;
import java.util.Scanner;

public class Scanner_Echo {
    public static void main(String[] args) {
        System.out.println("Podaj swoje imie i nazwisko");
        Scanner scanner= new Scanner(System.in);
        String imie = scanner.nextLine();
        System.out.println("Siemaneczko " + imie);

    }
}
