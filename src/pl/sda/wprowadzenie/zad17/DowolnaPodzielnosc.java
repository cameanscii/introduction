package pl.sda.wprowadzenie.zad17;

public class DowolnaPodzielnosc {
    public static void main(String[] args) {
        System.out.println(metodaDzielna(12,7));
    }

    public static boolean metodaDzielna (int a, int dzielnik){
        return a%dzielnik == 0;
    }
}
