package pl.sda.wprowadzenie.zad12;

import java.util.Random;
import java.util.Scanner;

public class Gra_Zgadywanka {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();
        int wygenerowana = generator.nextInt(100);

        int liczbaOdUzytkownika;
        do{
            System.out.println("Podaj liczbę: ");
            liczbaOdUzytkownika = scanner.nextInt();
            if (liczbaOdUzytkownika<wygenerowana){
                System.out.println("Podaj wieksza liczbe");
            } else if(liczbaOdUzytkownika>wygenerowana){
                System.out.println("Podaj mniejsza liczbe");
            }
        }while(liczbaOdUzytkownika != wygenerowana);
        System.out.println("BRAWO! Wygrałeś! Wygenerowana liczba to " + wygenerowana);
    }
}
